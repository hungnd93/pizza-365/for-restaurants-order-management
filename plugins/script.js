"use strict";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const gCOMBO_SMALL = "S";
    const gCOMBO_MEDIUM = "M";
    const gCOMBO_LARGE = "L";
    const gPIZZA_HAWAII = "Hawaii";
    const gPIZZA_SEAFOOD = "seafood";
    const gPIZZA_BACON = "Bacon";
    // mảng cục bộ chứa đối tượng 
    var gOrderObj = [];
    // biến cục bộ lưu value Id
    var gId = "";
    //biến cục bộ lưu value orderId
    var gOrderId = "";
    // Mảng chứa dữ liệu combo
    var gCOMBO_MENU = [
        { value: "S", text: "Small" },
        { value: "M", text: "Medium" },
        { value: "L", text: "Large" }
    ];
    // Mảng chứa dữ liệu pizza
    var gPIZZA_OBJ = [
        { value: "Hawaii", text: "Hawaii" },
        { value: "Seafood", text: "Hải sản" },
        { value: "Bacon", text: "Thịt hun khói" }
    ];
    // Mảng chưa dữ liệu trạng thái
    var gSTATUS_OBJ = [
        { value: "open", text: "Open" },
        { value: "cancel", text: "Cancel" },
        { value: "confirmed", text: "Confirmed" }
    ];

    const gARR_NAME = ["orderCode", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"];
    const gCOL_ID = 0;
    const gCOL_KICH_CO_COMBO = 1;
    const gCOL_LOAI_PIZZA = 2;
    const gCOL_NUOC_UONG = 3;
    const gCOL_THANH_TIEN = 4;
    const gCOL_HO_VA_TEN = 5;
    const gCOL_SO_DIEN_THOAI = 6;
    const gCOL_TRANG_THAI = 7;
    const gCOL_CHI_TIET = 8;
    // định nghĩa table
    var gOrderTable = $("#order-table").DataTable({
        columns: [
            { data: gARR_NAME[gCOL_ID] },
            { data: gARR_NAME[gCOL_KICH_CO_COMBO] },
            { data: gARR_NAME[gCOL_LOAI_PIZZA] },
            { data: gARR_NAME[gCOL_NUOC_UONG] },
            { data: gARR_NAME[gCOL_THANH_TIEN] },
            { data: gARR_NAME[gCOL_HO_VA_TEN] },
            { data: gARR_NAME[gCOL_SO_DIEN_THOAI] },
            { data: gARR_NAME[gCOL_TRANG_THAI] },
            { data: gARR_NAME[gCOL_CHI_TIET] }
        ],
        columnDefs: [
            {
                targets: gCOL_CHI_TIET,
                defaultContent:
                    `<button class="btn btn-success btn-edit" title="Chi tiết"><i class="far fa-edit"></i></button>
                <button class="btn btn-danger btn-delete" title="Chi tiết"><i class="far fa-trash-alt"></i></button>`
            }
        ]
    })
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    // gán sự kiện cho nút bấm(incon) chi tiết - update
    $("#order-table").on("click", ".btn-edit", function () {
        onBtnEditClick(this);
    });
    // gán sự kiện cho nút confirm order-modal update
    $("#confirm-modal-update").click(function () {
        onBtnConfirmModalUpdateClick();
    });
    // gán sự kiện cho nút bấm(icon) xóa order
    $("#order-table").on("click", ".btn-delete", function () {
        onBtnDeleteClick(this);
    });
    // gán sự kiện cho nút bấm xác nhận xóa order delete - modal delete
    $("#confirm-modal-delete").click(function () {
        onBtnConfirmDeleteClick();
    })
    // gán sự kiện cho nút create order
    $("#create-order").click(function () {
        onBtnCreateOrderClick();
    });
    // gán sự kiện cho nút confirm create order - modal create
    $("#modal-create-confirm").click(function () {
        onBtnConfirmCreateOrderClick();
    });
    // gán sự kiện cho nút lọc dữ liệu
    $("#btn-filter-data").click(function () {
        onBtnFilterOrderClick();
    });
    // gán sự kiện cho select element chọn combo -modal create order
    $("#select-combo-modal-create").on("change", function () {
        getComboSelectedClick();
    });
    // gán sự kiện cho select element chọn pizza - modal create order
    $("#select-loai-pizza-modal-create").on("change", function () {
        getPizzaSelectedClick();
    });
    // gán sự kiện cho select elenment chọn đồ uống - modal create order
    $("#select-do-uong-modal-create").on("change", function () {
        console.log("Nước uống được chọn: " + this.value);
    });
    // gán sự kiện cho select elenment chọn trạng thái
    $("#select-order-status").on("change", function () {
        console.log("Trạng thái được chọn: " + this.value);
    });
    // gán sự kiện cho select elenment chọn pizza
    $("#select-pizza-type").on("change", function () {
        console.log("Pizza được chọn: " + this.value);
    });
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    // Hàm xử lý khi tải trang
    function onPageLoading() {
        "use strict";
        console.log("Load trang tại đây");
        // gọi server và load dữ liệu order ra table
        getAllOrder();
        // gọi hàm load dữ liệu vào select element trạng thái
        loadDataStatusToSelectElement(gSTATUS_OBJ);
        // gọi hàm load dữ liệu vào select element pizza
        loadDataPizzaToSelectElement(gPIZZA_OBJ);
    }
    // Hàm xử lý khi nhấn nút create order
    function onBtnCreateOrderClick() {
        "use strict";
        console.log("Button Create Order Clicked");
        // hiện modal - create order
        $("#modal-create-order").modal("show");
        // gọi hàm load dữ liệu combo vào select - modal create
        loadDataComboToModalCreate(gCOMBO_MENU);
        // gọi hàm load dữ liệu pizza vào select - modal create
        loadDataPizzaToModalCreate(gPIZZA_OBJ);
        // gọi hàm load dữ liệu đồ uống vào select - modal create
        getDrinkList();

    }
    // Hàm xử lý khi nhấn nút confirm tạo đơn hàng - modal create
    function onBtnConfirmCreateOrderClick() {
        "use strict";
        //B1: thu thập dữ liệu
        var vOrderObj = getDataCreateOrder();
        // định nghĩa đối tượng cần xử lý
        var vCreateOrderObj = {
            kichCo: vOrderObj.menuDuocChon.menuName,
            duongKinh: vOrderObj.menuDuocChon.duongKinhCM,
            suon: vOrderObj.menuDuocChon.suonNuong,
            salad: vOrderObj.menuDuocChon.saladGr,
            soLuongNuoc: vOrderObj.menuDuocChon.drink,
            thanhTien: vOrderObj.menuDuocChon.priceVND,
            loaiPizza: vOrderObj.loaiPizza.pizzaDuocChon,
            idVourcher: vOrderObj.voucher,
            idLoaiNuocUong: vOrderObj.loaiNuocUong,
            hoTen: vOrderObj.hoVaTen,
            email: vOrderObj.email,
            soDienThoai: vOrderObj.dienThoai,
            diaChi: vOrderObj.diaChi,
            loiNhan: vOrderObj.loiNhan,
            trangThai: vOrderObj.trangThai
        };
        console.log(vCreateOrderObj);
        // B2: Kiểm tra dữ liệu
        var vDuLieuHopLe = validataDataCreateOrder(vCreateOrderObj);
        if (vDuLieuHopLe == true) {
            // B3: gọi server tạo mới đơn hàng và xử lý hiển thị
            createNewOrder(vCreateOrderObj);
        }
    }
    // Hàm xử lý khi nhấn nút lọc dữ liệu
    function onBtnFilterOrderClick() {
        "use strict";
        console.log("Nút lọc dữ liệu được ấn");
        // định nghĩa đối tượng cần lọc dữ liệu
        var vFilterOrderObj = {
            trangThai: "",
            loaiPizza: ""
        };
        // B1: Thu thập dữ liệu
        getDataOrderFiltered(vFilterOrderObj);
        //B2: Kiểm tra dữ liệu(không cần);
        //B3: Lọc và hiển thị dữ liệu ra bảng
        filterDataOrder(vFilterOrderObj);
    }
    // Hàm xử lý khi nút chi tiết được ấn
    function onBtnEditClick(paramElementButton) {
        "use strict";
        console.log("Nút chi tiết được ấn");

        // hiện modal chi tiết đơn hàng
        $("#modal-detail").modal('show');
        // load dữ liệu status vào select element
        loadDataStatusToModalUpdate(gSTATUS_OBJ);
        //B1: Thu thập dữ liệu
        var vOrderObj = getDataOrderRowCurrent(paramElementButton);
        gId = vOrderObj.id;
        console.log("gId = " + gId);
        gOrderId = vOrderObj.orderCode;
        console.log("gOrderId = " + gOrderId);
        //B2: Kiểm tra dữ liệu
        if (gOrderId == null) {
            alert("Không tìm thấy OrderID");
            return gOrderId;
        }
        else {
            //B3: gọi server và xử lý hiển thị
            getDataOrderByOrderId(gOrderId);
        }
    }
    // Hàm xử lý khi nhấn nút confirm-modal update
    function onBtnConfirmModalUpdateClick() {
        "use strict";
        console.log("Nút confirm được ấn");
        console.log("Id = " + gId);
        // truy vấn select status element
        var vStatusRequest = $("#select-status-modal-update").val()
        // định nghĩa đối tượng trạng thái
        var vObjectRequest = {
            trangThai: vStatusRequest,
        };
        // B1: Thu thập dữ liệu (không cần)
        // B2: Kiểm tra dữ liệu (không cần)
        // B3: gọi server update đơn hàng và xử lý hiển thị
        updateOrder(vObjectRequest);
    }
    // Hàm xử lý khi button(icon) delete được ấn
    function onBtnDeleteClick(paramElementButton) {
        "use strict";
        console.log("Nút xóa được click");
        // hiện modal xác nhận xóa order
        $("#modal-delete").modal('show');
        // B1: thu thập dữ liệu Id cần xóa
        var vOrderObj = getDataOrderRowCurrent(paramElementButton);
        console.log(vOrderObj);
        gId = vOrderObj.id;
        console.log("gId = " + gId);
        // B2: Kiểm tra dữ liệu(không cần)  
    }
    // hàm xử lý khi nút xác nhận xóa order -modal delete
    function onBtnConfirmDeleteClick() {
        "use strict";
        // B1: thu thập dữ liệu (không cần)
        // B2: Kiểm tra dữ liệu(không cần);
        // B3: gọi sever xóa dữ liệu order và hiển thị
        deleteOrderData(gId);
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

    // Hàm gọi server lấy tất cả thông tin order
    function getAllOrder() {
        "use strict";
        $.ajax({
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders',
            method: 'GET',
            dataType: 'json',
            success: function (responseText) {
                console.log(responseText);
                gOrderObj = responseText;
                // gọi hàm load dữ liệu ra table
                loadDataOrderToTable(gOrderObj);
            },
            error: function (ajaxErro) {
                alert(ajaxErro.responseText);
            }
        })
    }
    // Hàm gọi server xử lý tạo mới đơn hàng
    function createNewOrder(paramOrderObj) {
        "use strict";
        $.ajax({
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders',
            method: 'POST',
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify(paramOrderObj),
            success: function (createOrder) {
                alert("Thêm mới đơn hàng thành công");
                // xóa trắng thông tin trên modal create
                clearDataModalCreateOrder();
                // ẩn modal create order
                $("#modal-create-order").modal("hide");
                // load lại trang
                window.location.href = "index.html";
            },
            error: function (ajaxErro) {
                alert(ajaxErro.responseText);
            }
        })
    }
    // Hàm gọi server update trạng thái đơn
    function updateOrder(paramOrderRequest) {
        "use strict";
        $.ajax({
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders/' + gId,
            method: 'PUT',
            data: JSON.stringify(paramOrderRequest),
            dataType: 'json',
            contentType: 'application/json;charset=UTF-8',
            async: false,
            success: function (responseText) {
                console.log(responseText);
                alert("Update trạng thái đơn thành công");
                // ẩn modal update - detail
                $("#modal-detail").modal('hide');
                // load lại trang
                window.location.href = "index.html";
            },
            error: function (ajaxErro) {
                alert(ajaxErro.responseText);
            }
        })
    }
    // Hàm xử lý lấy thông tin order by Id
    function getDataOrderByOrderId(paramOrderId) {
        "use strict";
        $.ajax({
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders/' + paramOrderId,
            method: 'GET',
            dataType: 'json',
            success: function (responseText) {
                console.log(responseText);
                // Load dữ liệu order ra form
                loadDataOrderDetailToForm(responseText);
            },
            error: function (ajaxErro) {
                alert(ajaxErro.responseText);
            }
        })
    }
    // Hàm xử lý xóa user
    function deleteOrderData(paramOrderId) {
        $.ajax({
            url: "http://203.171.20.210:8080/devcamp-pizza365/orders/" + paramOrderId,
            method: 'DELETE',
            data: paramOrderId,
            dataType: 'json', // added data type
            contentType: 'application/json;charset=UTF-8',
            success: function () {
                alert("Xóa đơn hàng thành công");
                // ẩn modal
                $("#modal-delete").modal("hide")
                // load lại trang
                window.location.href = "index.html"
            },
            error: function (ajaxErro) {
                alert(ajaxErro.responseText);
            }
        });
    }
    // Hàm lấy danh sách loại nước uống và load vào select
    function getDrinkList() {
        "use strict";
        $.ajax({
            url: 'http://203.171.20.210:8080/devcamp-pizza365/drinks',
            method: 'GET',
            dataType: 'json',
            success: function (drinkObj) {
                //load data nước uống vào select element
                loadDataDrinkToSelectElement(drinkObj);
            },
            error: function (ajaxErro) {
                alert(ajaxErro.responseText);
            }
        });
    }
    // Hàm thu thập dữ liệu(lấy thông tin combo)
    function getDataCombo(paramMenu, paramDuongKinh, paramSuonNuong, paramSalad, paramDrink, paramPriceVND) {
        "use strict";
        var vComboObj = {
            menuName: paramMenu,
            duongKinhCM: paramDuongKinh,
            suonNuong: paramSuonNuong,
            saladGr: paramSalad,
            drink: paramDrink,
            priceVND: paramPriceVND,
            // Phương thức (method) display in console
            displayInConsole() {
                console.log("%c---Combo được chọn ---", "color: orange");
                console.log("Kích cỡ: " + vComboObj.menuName);
                console.log("Đường kinh: " + vComboObj.duongKinhCM + "cm");
                console.log("Sườn nướng: " + vComboObj.suonNuong);
                console.log("Salad: " + vComboObj.saladGr + "g");
                console.log("Nước ngọt: " + vComboObj.drink);
                console.log("Giá: " + "VND " + vComboObj.priceVND);
            }
        }
        return vComboObj;
    }
    // Hàm thu thập lấy thông tin loại pizza
    function getDataPizza(paramPizza) {
        "use strict";
        var vPizzaObj = {
            pizzaDuocChon: paramPizza,
            displayInConsole() {
                console.log("Pizza được chọn: " + vPizzaObj.pizzaDuocChon);
            }
        }
        return vPizzaObj;
    }
    // Hàm thu thập dữ liệu thông tin order
    function getDataCreateOrder() {
        "use strict";
        // thu thập thông tin gói combo được chọn
        var vComboSelected = getComboSelectedClick();
        // thu thập thông tin loại pizza được chọn
        var vPizzaSelected = getPizzaSelectedClick();
        // truy vấn các phần tử input trên form
        var vSelectNuocUong = $("#select-do-uong-modal-create").val();
        var vInputHoTen = $.trim($("#inp-ho-ten-modal-create").val());
        var vInputEmail = $.trim($("#inp-email-modal-create").val());
        var vInputSoDienThoai = $.trim($("#inp-so-dien-thoai-modal-create").val());
        var vInputDiaChi = $.trim($("#inp-dia-chi-modal-create").val());
        var vInputMaGiamGia = $.trim($("#inp-vourcher-modal-create").val());
        var vInputLoiNhan = $.trim($("#inp-loi-nhan-modal-create").val());
        var vSelectTrangThaiDon = $("#select-trang-thai-modal-create").val();
        // Định nghĩa đối tượng lưu trữ thông tin đơn hàng
        var vOrderObj = {
            menuDuocChon: vComboSelected,
            loaiPizza: vPizzaSelected,
            loaiNuocUong: vSelectNuocUong,
            hoVaTen: vInputHoTen,
            email: vInputEmail,
            dienThoai: vInputSoDienThoai,
            diaChi: vInputDiaChi,
            voucher: vInputMaGiamGia,
            loiNhan: vInputLoiNhan,
            trangThai: vSelectTrangThaiDon,
            phanTramGiamGia: getVoucherObj(),
            priceAnnualVND: function () {
                return this.menuDuocChon.priceVND * (100 - getVoucherObj()) / 100;
            }
        };
        return vOrderObj;
    }
    // Hàm xử lý khi chọn combo select element
    function getComboSelectedClick() {
        "use strict";
        var vSelectCombo = $("#select-combo-modal-create").val(); // truy vấn select element chọn combo
        var vComboSelected = getDataCombo("", 0, 0, 0, 0, 0); // Biến lưu combo được chọn
        if (vSelectCombo === "S") {
            vComboSelected = getDataCombo(gCOMBO_SMALL, 20, 2, 200, 2, 150000);
            // Hiển thị gói thông tin ra console
            vComboSelected.displayInConsole();
        }
        else if (vSelectCombo === "M") {
            vComboSelected = getDataCombo(gCOMBO_MEDIUM, 25, 4, 300, 3, 200000);
            // Hiển thị gói thông tin ra console
            vComboSelected.displayInConsole();
        }
        else if (vSelectCombo === "L") {
            vComboSelected = getDataCombo(gCOMBO_LARGE, 30, 8, 500, 4, 250000);;
            // Hiển thị gói thông tin ra console
            vComboSelected.displayInConsole();
        }
        return vComboSelected;
    }
    // Hàm xử lý khi chọn pizza select element
    function getPizzaSelectedClick() {
        "use strict";
        var vSelectPizza = $("#select-loai-pizza-modal-create").val(); // truy vấn select element chọn pizza
        var vPizzaSelected = getDataPizza(""); // biến lưu pizza được chọn
        if (vSelectPizza === "Hawaii") {
            vPizzaSelected = getDataPizza(gPIZZA_HAWAII);
            // hiển thị loại pizza được chọn ra console
            vPizzaSelected.displayInConsole()
        }
        else if (vSelectPizza === "Seafood") {
            vPizzaSelected = getDataPizza(gPIZZA_SEAFOOD);
            // hiển thị loại pizza được chọn ra console
            vPizzaSelected.displayInConsole()
        }
        else if (vSelectPizza === "Bacon") {
            vPizzaSelected = getDataPizza(gPIZZA_BACON);
            // hiển thị loại pizza được chọn ra console
            vPizzaSelected.displayInConsole()
        }
        return vPizzaSelected;
    }
    // Hàm lấy giá trị phần trăm giảm giá
    function getVoucherObj() {
        // lấy giá trị trên form mã giảm giá
        var vInputMaGiamGia = $.trim($("#inp-vourcher-modal-create").val());
        var vVoucherDiscountPercent = 0; // biến lưu phần trăm giảm giá
        // tạo ra đối tượng request và gửi đi
        if (vInputMaGiamGia != "") {
            $.ajax({
                url: "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + vInputMaGiamGia,
                method: 'GET',
                dataType: 'json',
                async: false,
                success: function (maGiamGiaObj) {
                    vVoucherDiscountPercent = maGiamGiaObj.phanTramGiamGia;
                }
            });
        }
        return vVoucherDiscountPercent;
    }
    // Hàm kiểm tra dữ liệu tạo đơn hàng
    function validataDataCreateOrder(paramOrder) {
        "use strict";
        if (paramOrder.kichCo === "") {
            alert("Chưa chọn Combo");
            return false;
        }
        if (paramOrder.loaiPizza === "") {
            alert("Chưa chọn Pizza");
            return false;
        }
        if (paramOrder.idLoaiNuocUong === "NOT_SELECT_DRINK") {
            alert("Chưa chọn đồ uống");
            return false;
        }
        if (isNaN(paramOrder.idVourcher)) {
            alert("Voucher phải là số");
            return false;
        }
        if (paramOrder.hoTen === "") {
            alert("Chưa nhập họ tên");
            return false;
        }
        if (paramOrder.email !== "" && validateEmail(paramOrder.email) == false) {
            alert("Không đúng định dạng email");
            return false;
        }
        if (paramOrder.soDienThoai === "") {
            alert("Chưa nhập số điện thoại");
            return false;
        }
        if (validatePhone(paramOrder.soDienThoai) == false) {
            alert("Số điện thoại không đúng");
            return false;
        }
        if (paramOrder.diaChi === "") {
            alert("Chưa nhập địa chỉ");
            return false;
        }
        return true;
    }
    // hàm kiểm tra định dạng số điện thoại
    function validatePhone(number) {
        "use strict";
        var vformPhone = /(((\+|)84)|0)(3|5|7|8|9)+([0-9]{8})\b/;
        if (vformPhone.test(number)) {
            return true;
        }
        else {
            return false;
        }
    }
    // hàm kiểm tra định dạng email
    function validateEmail(paramEmail) {
        var atposition = paramEmail.indexOf("@");
        var dotposition = paramEmail.lastIndexOf(".");
        if (atposition < 1 || dotposition < (atposition + 2) || (dotposition + 2) >= paramEmail.length) {
            return false;
        }
        return true;
    }
    // hàm load dữ liệu ra table
    function loadDataOrderToTable(paramOrderObj) {
        "use strict";
        gOrderTable.clear();// xóa trắng bảng
        gOrderTable.rows.add(paramOrderObj);// cập nhật data cho bảng
        gOrderTable.draw();// cập nhập hiển thị lại giao diện bảng
    }
    // Hàm lấy thông tin đơn hàng khi nhấn nút upate (icon update)
    function getDataOrderRowCurrent(paramElementButton) {
        "use strict";
        var vRowCurrent = $(paramElementButton).closest("tr");
        var vRowData = gOrderTable.row(vRowCurrent).data();
        // gán giá trị id và orderId vào biến cục bộ
        return vRowData;
    }
    // Hàm xử lý load dữ liệu từ order vào modal detail
    function loadDataOrderDetailToForm(paramOrder) {
        "use strict";
        $("#inp-order-code-modal-update").val(paramOrder.orderCode);
        $("#inp-combo-modal-update").val(paramOrder.kichCo);
        $("#inp-duong-kinh-modal-update").val(paramOrder.duongKinh + "cm");
        $("#inp-suon-nuong-modal-update").val(paramOrder.suon + " miếng");
        $("#inp-do-uong-modal-update").val(paramOrder.idLoaiNuocUong);
        $("#inp-so-luong-nuoc-modal-update").val(paramOrder.soLuongNuoc);
        $("#inp-vourcher-modal-update").val(paramOrder.idVourcher);
        $("#inp-loai-pizza-modal-update").val(paramOrder.loaiPizza);
        $("#inp-salad-modal-update").val(paramOrder.salad + "gr");
        $("#inp-thanh-tien-modal-update").val(paramOrder.thanhTien + " VNĐ");
        $("#inp-giam-gia-modal-update").val(paramOrder.giamGia + " VNĐ");
        $("#inp-ho-ten-modal-update").val(paramOrder.hoTen);
        $("#inp-email-modal-update").val(paramOrder.email);
        $("#inp-so-dien-thoai-modal-update").val(paramOrder.soDienThoai);
        $("#inp-dia-chi-modal-update").val(paramOrder.diaChi);
        $("#inp-loi-nhan-modal-update").val(paramOrder.loiNhan);
        $("#select-status-modal-update").val(paramOrder.trangThai);
        $("#inp-ngay-tao-don-modal-update").val(paramOrder.ngayTao);
        $("#inp-ngay-cap-nhat-modal-update").val(paramOrder.ngayCapNhat);
    }

    // Hàm thu thập dữ liệu đối tượng order cần lọc
    function getDataOrderFiltered(paramOrderFilter) {
        "use strict";
        paramOrderFilter.trangThai = $("#select-order-status").val();
        paramOrderFilter.loaiPizza = $("#select-pizza-type").val();
    }
    // Hàm lọc đối tượng order và đổ dữ liệu vào bảng
    function filterDataOrder(paramOrderFilter) {
        "use strict";
        var vDataFilter = gOrderObj.filter(function (paramOrderObj, index) {
            return (paramOrderFilter.trangThai === "NOT_SELECT_STATUS" || paramOrderFilter.trangThai === paramOrderObj.trangThai)
                && (paramOrderFilter.loaiPizza === "NOT_SELECT_PIZZA" || paramOrderFilter.loaiPizza === paramOrderObj.loaiPizza)
        });
        // gọi hàm load dữ liệu vào bảng
        loadDataOrderToTable(vDataFilter);
    }
    // Hàm đổ dữ liệu vào select chọn trạng thái
    function loadDataStatusToSelectElement(paramStatus) {
        "use strict";
        var vStatusSelect = $("#select-order-status");
        $("<option/>", {
            text: "Chọn trạng thái",
            value: "NOT_SELECT_STATUS"
        }).appendTo(vStatusSelect);

        for (var bI = 0; bI < paramStatus.length; bI++) {
            var bStatusOption = $("<option/>", {
                text: paramStatus[bI].text,
                value: paramStatus[bI].value
            }).appendTo(vStatusSelect);
        }
    }
    // Hàm đổ dữ liệu vào select chọn pizza
    function loadDataPizzaToSelectElement(paramPizza) {
        "use strict";
        var vPizzaSelect = $("#select-pizza-type");
        $("<option/>", {
            text: "Chọn loại pizza",
            value: "NOT_SELECT_PIZZA"
        }).appendTo(vPizzaSelect);

        for (var bI = 0; bI < paramPizza.length; bI++) {
            var bPizzaOption = $("<option/>", {
                text: paramPizza[bI].text,
                value: paramPizza[bI].value
            }).appendTo(vPizzaSelect);
        }
    }
    // Hàm đổ dữ liệu vào select drink
    function loadDataDrinkToSelectElement(paramDrinkObj) {
        "use strict";
        var vDrinkSelect = $("#select-do-uong-modal-create");
        $("<option/>", {
            text: "Chọn đồ uống",
            value: "NOT_SELECT_DRINK"
        }).appendTo(vDrinkSelect);

        for (var bI = 0; bI < paramDrinkObj.length; bI++) {
            var bDrinkOption = $("<option/>", {
                text: paramDrinkObj[bI].tenNuocUong,
                value: paramDrinkObj[bI].maNuocUong
            }).appendTo(vDrinkSelect);
        }
    }
    // Hàm đổ dữ liệu vào select chọn combo -modal create
    function loadDataComboToModalCreate(paramCombo) {
        "use strict";
        var vComboSelectMoldalCreate = $("#select-combo-modal-create");
        $("<option/>", {
            text: "Chọn combo",
            value: "NOT_SELECT_COMBO"
        }).appendTo(vComboSelectMoldalCreate);

        for (var bI = 0; bI < paramCombo.length; bI++) {
            var bComboOptionMoldalCreate = $("<option/>", {
                text: paramCombo[bI].text,
                value: paramCombo[bI].value
            }).appendTo(vComboSelectMoldalCreate);
        }
    }
    // Hàm đổ dữ liệu vào select chọn pizza - moldal create
    function loadDataPizzaToModalCreate(paramPizza) {
        "use strict";
        var vPizzaSelectMoldalCreate = $("#select-loai-pizza-modal-create");
        $("<option/>", {
            text: "Chọn loại pizza",
            value: "NOT_SELECT_PIZZA"
        }).appendTo(vPizzaSelectMoldalCreate);

        for (var bI = 0; bI < paramPizza.length; bI++) {
            var bPizzaOptionMoldalCreate = $("<option/>", {
                text: paramPizza[bI].text,
                value: paramPizza[bI].value
            }).appendTo(vPizzaSelectMoldalCreate);
        }
    }
    // Hàm đổ dữ liệu vào select chọn trạng thái
    function loadDataStatusToModalUpdate(paramStatus) {
        "use strict";
        var vStatusSelectModalUpdate = $("#select-status-modal-update");
        for (var bI = 0; bI < paramStatus.length; bI++) {
            var bStatusOptionModalUpdate = $("<option/>", {
                text: paramStatus[bI].text,
                value: paramStatus[bI].value
            }).appendTo(vStatusSelectModalUpdate);
        }
    }
    // Hàm xóa trắng thông tin modal create order
    function clearDataModalCreateOrder() {
        "use strict";
        $("#select-combo-modal-create").val("NOT_SELECT_COMBO")
        $("#select-loai-pizza-modal-create").val("NOT_SELECT_PIZZA");
        $("#select-do-uong-modal-create").val("NOT_SELECT_DRINK");
        $("#inp-ho-ten-modal-create").val('');
        $("#inp-email-modal-create").val('');
        $("#inp-so-dien-thoai-modal-create").val('');
        $("#inp-dia-chi-modal-create").val('');
        $("#inp-vourcher-modal-create").val('');
        $("#inp-loi-nhan-modal-create").val('');
    }
});